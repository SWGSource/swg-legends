/**********************************************************************

 * Title:        starport.script
 * Description:  handles the travel location functionality on starports

 **********************************************************************/

include library.city;
include library.travel;

inherits structure.municipal.starport;

trigger OnAttach()
{
	messageTo( self, "setupStartport", null, 1.f, false );

	return super.OnAttach(self);
}

trigger OnInitialize()
{
	// Add the structure's travel point.
	string planet = getCurrentSceneName();
	string travel_point = travel.getTravelPointName( self );
	location arrival_loc = travel.getArrivalLocation( self );
	int travel_cost = travel.getTravelCost( self );

	if ( travel_point == null || travel_cost == -1 )
		return super.OnInitialize(self);

	travel.initializeStarport( self, travel_point, travel_cost, true );

	return super.OnInitialize(self);
}

messageHandler setupStartport()
{
	// Get city info.
	int city_id = getCityAtLocation( getLocation( self ), 0 );
	obj_id mayor = cityGetLeader( city_id );

	// Do we already have a shuttleport?
	int cost = cityGetTravelCost( city_id );
	if ( cost > 0 )
	{
		// CHEATERS!  They somehow were able to almost place a 2nd shuttleport.
		// Notify the mayor that this happened.

		// DIE!
		destroyObject( self );
		return SCRIPT_CONTINUE;
	}

	// Set an initial object name.
	string cityName = cityGetName( city_id );
	string travel_point = cityName;

	// Set an initial cost.
	int travel_cost = 100;

	// Do it!
	travel.initializeStarport( self, travel_point, travel_cost, true );

	return SCRIPT_CONTINUE;
}

trigger OnDestroy()
{
	// We need to clean up our travel objects.
	boolean initd = false;
	obj_id[] objects = getObjIdArrayObjVar( self, "travel.base_object" );
	for ( int i=0; i<objects.length; i++ )
	{
		destroyObject( objects[i] );
		initd = true;
	}

	// Get rid of our travel location.
	if ( initd )
		city.removeStarport( self );

	return super.OnInitialize(self);
}
