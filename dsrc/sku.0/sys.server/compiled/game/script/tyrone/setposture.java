package script.developer;

import script.*;
import script.base_class.*;
import script.combat_engine.*;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;
import script.base_script;

import script.library.utils;

public class setposture extends script.base_script
{
    public setposture()
    {
    }
    public int OnAttach(obj_id self) throws InterruptedException
    {
        sendSystemMessageTestingOnly(self, "Posture script attached.");
        return SCRIPT_CONTINUE;
    }
    public int OnHearSpeech(obj_id self, obj_id speaker, String text) throws InterruptedException
    {
        if (speaker != self)
        {
            return SCRIPT_CONTINUE;
        }
        String[] argv = split(text, ' ');
        if (argv[0].equals("list"))
        {
            sendSystemMessageTestingOnly(self, "none, upright, crouched, prone, sneaking, blocking, climbing, flying, lying_down, sitting, skill_animating, driving, mounted, kd, incapped ");
        }
        if (argv[0].equals("none"))
        {
            setPosture(self, POSTURE_NONE);
        }
        if (argv[0].equals("upright"))
        {
            setPosture(self, POSTURE_UPRIGHT);
        }
        if (argv[0].equals("crouched"))
        {
            setPosture(self, POSTURE_CROUCHED);
        }
        if (argv[0].equals("prone"))
        {
            setPosture(self, POSTURE_PRONE);
        }
        if (argv[0].equals("sneaking"))
        {
            setPosture(self, POSTURE_SNEAKING);
        }
        if (argv[0].equals("blocking"))
        {
            setPosture(self, POSTURE_BLOCKING);
        }
        if (argv[0].equals("climbing"))
        {
            setPosture(self, POSTURE_CLIMBING);
        }
        if (argv[0].equals("flying"))
        {
            setPosture(self, POSTURE_FLYING);
        }
        if (argv[0].equals("lying_down"))
        {
            setPosture(self, POSTURE_LYING_DOWN);
        }
        if (argv[0].equals("sitting"))
        {
            setPosture(self, POSTURE_SITTING);
        }
        if (argv[0].equals("skill_animating"))
        {
            setPosture(self, POSTURE_SKILL_ANIMATING);
        }
        if (argv[0].equals("driving"))
        {
            setPosture(self, POSTURE_DRIVING_VEHICLE);
        }
        if (argv[0].equals("mounted"))
        {
            setPosture(self, POSTURE_RIDING_CREATURE);
        }
        if (argv[0].equals("kd"))
        {
            setPosture(self, POSTURE_KNOCKED_DOWN);
        }
        if (argv[0].equals("incapped"))
        {
            setPosture(self, POSTURE_INCAPACITATED);
        }
        return SCRIPT_CONTINUE;
    }
}