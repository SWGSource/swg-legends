package script.tyrone;

import script.*;
import script.base_class.*;
import script.combat_engine.*;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;
import script.base_script;

import script.library.utils;
import script.library.anims;
import script.library.space_transition;
import script.library.create;
import script.library.chat;
import script.library.hue;
import script.library.npe;
import script.library.groundquests;
import script.library.pet_lib;
import script.library.ship_ai;
import script.library.space_utils;
import script.library.space_combat;
import script.library.space_create;
import script.library.sui;

public class tyrone_debug extends script.base_script
{
    public tyrone_debug()
    {
    }
	public static final String[] TYRONECOMMANDTITLE = 
	{
		"Tyrone's Debug Script: Commands"
	};
	public static final String[] TYRONECOMMANDPROMPT = 
    {
        " grantAllSkillsCombat -- Grants all professions that are combat related. (excludes entertainer).",
        " grantAllSkillsEntertainer -- Grants Entertainer.",
        " grantAllSkillsCrafting -- Grants All four crafting professions.",
        " setInteresting -- Sets Look-at target to have Interesting Disk above object/creature.",
        " clearInteresting -- Clears Look-at target's Interesting Disk above object/creature.",
		" setSpaceInteresting -- Sets Look-at target to have Space Interesting Disk above object/creature.",
        " clearSpaceInteresting -- Clears Look-at target's Space Interesting Disk above object/creature.",
		" MakeTyroneADroid -- Makes a crafting droid with datapad and inventory storage.",
		" tyronesTestQuests -- Grant Tyrone's testing quests (requires client patch).",
		" persist -- persist your look-at target",
		" chatTest -- Make someone say (Hello!) and curtsey."
    };
    public static final String[] JEDI = 
    {
        "class_forcesensitive_phase1",
        "class_forcesensitive_phase1_novice",
        "class_forcesensitive_phase1_02",
        "class_forcesensitive_phase1_03",
        "class_forcesensitive_phase1_04",
        "class_forcesensitive_phase1_05",
        "class_forcesensitive_phase1_master",
        "class_forcesensitive_phase2",
        "class_forcesensitive_phase2_novice",
        "class_forcesensitive_phase2_02",
        "class_forcesensitive_phase2_03",
        "class_forcesensitive_phase2_04",
        "class_forcesensitive_phase2_05",
        "class_forcesensitive_phase2_master",
        "class_forcesensitive_phase3",
        "class_forcesensitive_phase3_novice",
        "class_forcesensitive_phase3_02",
        "class_forcesensitive_phase3_03",
        "class_forcesensitive_phase3_04",
        "class_forcesensitive_phase3_05",
        "class_forcesensitive_phase3_master",
        "class_forcesensitive_phase4",
        "class_forcesensitive_phase4_novice",
        "class_forcesensitive_phase4_02",
        "class_forcesensitive_phase4_03",
        "class_forcesensitive_phase4_04",
        "class_forcesensitive_phase4_05",
        "class_forcesensitive_phase4_master"
    };
    public static final String[] SMUGGLER = 
    {
        "class_smuggler_phase1",
        "class_smuggler_phase1_novice",
        "class_smuggler_phase1_02",
        "class_smuggler_phase1_03",
        "class_smuggler_phase1_04",
        "class_smuggler_phase1_05",
        "class_smuggler_phase1_master",
        "class_smuggler_phase2",
        "class_smuggler_phase2_novice",
        "class_smuggler_phase2_02",
        "class_smuggler_phase2_03",
        "class_smuggler_phase2_04",
        "class_smuggler_phase2_05",
        "class_smuggler_phase2_master",
        "class_smuggler_phase3",
        "class_smuggler_phase3_novice",
        "class_smuggler_phase3_02",
        "class_smuggler_phase3_03",
        "class_smuggler_phase3_04",
        "class_smuggler_phase3_05",
        "class_smuggler_phase3_master",
        "class_smuggler_phase4",
        "class_smuggler_phase4_novice",
        "class_smuggler_phase4_02",
        "class_smuggler_phase4_03",
        "class_smuggler_phase4_04",
        "class_smuggler_phase4_05",
        "class_smuggler_phase4_master"
    };
    public static final String[] SPY = 
    {
        "class_spy_phase1",
        "class_spy_phase1_novice",
        "class_spy_phase1_02",
        "class_spy_phase1_03",
        "class_spy_phase1_04",
        "class_spy_phase1_05",
        "class_spy_phase1_master",
        "class_spy_phase2",
        "class_spy_phase2_novice",
        "class_spy_phase2_02",
        "class_spy_phase2_03",
        "class_spy_phase2_04",
        "class_spy_phase2_05",
        "class_spy_phase2_master",
        "class_spy_phase3",
        "class_spy_phase3_novice",
        "class_spy_phase3_02",
        "class_spy_phase3_03",
        "class_spy_phase3_04",
        "class_spy_phase3_05",
        "class_spy_phase3_master",
        "class_spy_phase4",
        "class_spy_phase4_novice",
        "class_spy_phase4_02",
        "class_spy_phase4_03",
        "class_spy_phase4_04",
        "class_spy_phase4_05",
        "class_spy_phase4_master"
    };
    public static final String[] BH = 
    {
        "class_bountyhunter_phase1",
        "class_bountyhunter_phase1_novice",
        "class_bountyhunter_phase1_02",
        "class_bountyhunter_phase1_03",
        "class_bountyhunter_phase1_04",
        "class_bountyhunter_phase1_05",
        "class_bountyhunter_phase1_master",
        "class_bountyhunter_phase2",
        "class_bountyhunter_phase2_novice",
        "class_bountyhunter_phase2_02",
        "class_bountyhunter_phase2_03",
        "class_bountyhunter_phase2_04",
        "class_bountyhunter_phase2_05",
        "class_bountyhunter_phase2_master",
        "class_bountyhunter_phase3",
        "class_bountyhunter_phase3_novice",
        "class_bountyhunter_phase3_02",
        "class_bountyhunter_phase3_03",
        "class_bountyhunter_phase3_04",
        "class_bountyhunter_phase3_05",
        "class_bountyhunter_phase3_master",
        "class_bountyhunter_phase4",
        "class_bountyhunter_phase4_novice",
        "class_bountyhunter_phase4_02",
        "class_bountyhunter_phase4_03",
        "class_bountyhunter_phase4_04",
        "class_bountyhunter_phase4_05",
        "class_bountyhunter_phase4_master"
    };
    public static final String[] OFFICER = 
    {
        "class_officer_phase1",
        "class_officer_phase1_novice",
        "class_officer_phase1_02",
        "class_officer_phase1_03",
        "class_officer_phase1_04",
        "class_officer_phase1_05",
        "class_officer_phase1_master",
        "class_officer_phase2",
        "class_officer_phase2_novice",
        "class_officer_phase2_02",
        "class_officer_phase2_03",
        "class_officer_phase2_04",
        "class_officer_phase2_05",
        "class_officer_phase2_master",
        "class_officer_phase3",
        "class_officer_phase3_novice",
        "class_officer_phase3_02",
        "class_officer_phase3_03",
        "class_officer_phase3_04",
        "class_officer_phase3_05",
        "class_officer_phase3_master",
        "class_officer_phase4",
        "class_officer_phase4_novice",
        "class_officer_phase4_02",
        "class_officer_phase4_03",
        "class_officer_phase4_04",
        "class_officer_phase4_05",
        "class_officer_phase4_master"
    };
    public static final String[] MEDIC = 
    {
        "class_medic_phase1",
        "class_medic_phase1_novice",
        "class_medic_phase1_02",
        "class_medic_phase1_03",
        "class_medic_phase1_04",
        "class_medic_phase1_05",
        "class_medic_phase1_master",
        "class_medic_phase2",
        "class_medic_phase2_novice",
        "class_medic_phase2_02",
        "class_medic_phase2_03",
        "class_medic_phase2_04",
        "class_medic_phase2_05",
        "class_medic_phase2_master",
        "class_medic_phase3",
        "class_medic_phase3_novice",
        "class_medic_phase3_02",
        "class_medic_phase3_03",
        "class_medic_phase3_04",
        "class_medic_phase3_05",
        "class_medic_phase3_master",
        "class_medic_phase4",
        "class_medic_phase4_novice",
        "class_medic_phase4_02",
        "class_medic_phase4_03",
        "class_medic_phase4_04",
        "class_medic_phase4_05",
        "class_medic_phase4_master"
    };
    public static final String[] COMMANDO = 
    {
        "class_commando_phase1",
        "class_commando_phase1_novice",
        "class_commando_phase1_02",
        "class_commando_phase1_03",
        "class_commando_phase1_04",
        "class_commando_phase1_05",
        "class_commando_phase1_master",
        "class_commando_phase2",
        "class_commando_phase2_novice",
        "class_commando_phase2_02",
        "class_commando_phase2_03",
        "class_commando_phase2_04",
        "class_commando_phase2_05",
        "class_commando_phase2_master",
        "class_commando_phase3",
        "class_commando_phase3_novice",
        "class_commando_phase3_02",
        "class_commando_phase3_03",
        "class_commando_phase3_04",
        "class_commando_phase3_05",
        "class_commando_phase3_master",
        "class_commando_phase4",
        "class_commando_phase4_novice",
        "class_commando_phase4_02",
        "class_commando_phase4_03",
        "class_commando_phase4_04",
        "class_commando_phase4_05",
        "class_commando_phase4_master"
    };
    public static final String[] ENTERTAINER = 
    {
        "class_entertainer_phase1",
        "class_entertainer_phase1_novice",
        "class_entertainer_phase1_02",
        "class_entertainer_phase1_03",
        "class_entertainer_phase1_04",
        "class_entertainer_phase1_05",
        "class_entertainer_phase1_master",
        "class_entertainer_phase2",
        "class_entertainer_phase2_novice",
        "class_entertainer_phase2_02",
        "class_entertainer_phase2_03",
        "class_entertainer_phase2_04",
        "class_entertainer_phase2_05",
        "class_entertainer_phase2_master",
        "class_entertainer_phase3",
        "class_entertainer_phase3_novice",
        "class_entertainer_phase3_02",
        "class_entertainer_phase3_03",
        "class_entertainer_phase3_04",
        "class_entertainer_phase3_05",
        "class_entertainer_phase3_master",
        "class_entertainer_phase4",
        "class_entertainer_phase4_novice",
        "class_entertainer_phase4_02",
        "class_entertainer_phase4_03",
        "class_entertainer_phase4_04",
        "class_entertainer_phase4_05",
        "class_entertainer_phase4_master"
    };
    public static final String[] DOMESTICS = 
    {
        "class_domestics_phase1",
        "class_domestics_phase1_novice",
        "class_domestics_phase1_02",
        "class_domestics_phase1_03",
        "class_domestics_phase1_04",
        "class_domestics_phase1_05",
        "class_domestics_phase1_master",
        "class_domestics_phase2",
        "class_domestics_phase2_novice",
        "class_domestics_phase2_02",
        "class_domestics_phase2_03",
        "class_domestics_phase2_04",
        "class_domestics_phase2_05",
        "class_domestics_phase2_master",
        "class_domestics_phase3",
        "class_domestics_phase3_novice",
        "class_domestics_phase3_02",
        "class_domestics_phase3_03",
        "class_domestics_phase3_04",
        "class_domestics_phase3_05",
        "class_domestics_phase3_master",
        "class_domestics_phase4",
        "class_domestics_phase4_novice",
        "class_domestics_phase4_02",
        "class_domestics_phase4_03",
        "class_domestics_phase4_04",
        "class_domestics_phase4_05",
        "class_domestics_phase4_master"
    };
    public static final String[] MUNITIONS = 
    {
        "class_munitions_phase1",
        "class_munitions_phase1_novice",
        "class_munitions_phase1_02",
        "class_munitions_phase1_03",
        "class_munitions_phase1_04",
        "class_munitions_phase1_05",
        "class_munitions_phase1_master",
        "class_munitions_phase2",
        "class_munitions_phase2_novice",
        "class_munitions_phase2_02",
        "class_munitions_phase2_03",
        "class_munitions_phase2_04",
        "class_munitions_phase2_05",
        "class_munitions_phase2_master",
        "class_munitions_phase3",
        "class_munitions_phase3_novice",
        "class_munitions_phase3_02",
        "class_munitions_phase3_03",
        "class_munitions_phase3_04",
        "class_munitions_phase3_05",
        "class_munitions_phase3_master",
        "class_munitions_phase4",
        "class_munitions_phase4_novice",
        "class_munitions_phase4_02",
        "class_munitions_phase4_03",
        "class_munitions_phase4_04",
        "class_munitions_phase4_05",
        "class_munitions_phase4_master"
    };
    public static final String[] ENGINEER = 
    {
        "class_engineering_phase1",
        "class_engineering_phase1_novice",
        "class_engineering_phase1_02",
        "class_engineering_phase1_03",
        "class_engineering_phase1_04",
        "class_engineering_phase1_05",
        "class_engineering_phase1_master",
        "class_engineering_phase2",
        "class_engineering_phase2_novice",
        "class_engineering_phase2_02",
        "class_engineering_phase2_03",
        "class_engineering_phase2_04",
        "class_engineering_phase2_05",
        "class_engineering_phase2_master",
        "class_engineering_phase3",
        "class_engineering_phase3_novice",
        "class_engineering_phase3_02",
        "class_engineering_phase3_03",
        "class_engineering_phase3_04",
        "class_engineering_phase3_05",
        "class_engineering_phase3_master",
        "class_engineering_phase4",
        "class_engineering_phase4_novice",
        "class_engineering_phase4_02",
        "class_engineering_phase4_03",
        "class_engineering_phase4_04",
        "class_engineering_phase4_05",
        "class_engineering_phase4_master"
    };
    public static final String[] STRUCTURE = 
    {
        "class_structures_phase1",
        "class_structures_phase1_novice",
        "class_structures_phase1_02",
        "class_structures_phase1_03",
        "class_structures_phase1_04",
        "class_structures_phase1_05",
        "class_structures_phase1_master",
        "class_structures_phase2",
        "class_structures_phase2_novice",
        "class_structures_phase2_02",
        "class_structures_phase2_03",
        "class_structures_phase2_04",
        "class_structures_phase2_05",
        "class_structures_phase2_master",
        "class_structures_phase3",
        "class_structures_phase3_novice",
        "class_structures_phase3_02",
        "class_structures_phase3_03",
        "class_structures_phase3_04",
        "class_structures_phase3_05",
        "class_structures_phase3_master",
        "class_structures_phase4",
        "class_structures_phase4_novice",
        "class_structures_phase4_02",
        "class_structures_phase4_03",
        "class_structures_phase4_04",
        "class_structures_phase4_05",
        "class_structures_phase4_master"
    };
    public int OnAttach(obj_id self) throws InterruptedException
    {
        sendSystemMessageTestingOnly(self, "tyrone's script attached: tyrone.tyrone_debug");
        return SCRIPT_CONTINUE;
    }
    public int OnLogout(obj_id self) throws InterruptedException
    {
        debugServerConsoleMsg(self, "I logged out!");
        return SCRIPT_CONTINUE;
    }
    public int OnHearSpeech(obj_id self, obj_id objSpeaker, String strText) throws InterruptedException
    {
        if (objSpeaker != self)
        {
            return SCRIPT_CONTINUE;
        }
		if (strText.equals("tyroneCommands"))
		{
				String allHelpData = "";
                Arrays.sort(TYRONECOMMANDPROMPT);
                for (int i = 0; i < TYRONECOMMANDPROMPT.length; i++)
                {
                    allHelpData = allHelpData + TYRONECOMMANDPROMPT[i] + "\r\n\t";
                }
                //saveTextOnClient(self, "debugTyroneCommandUsage.txt", allHelpData);
				playMusic(self, "sound/sys_comm_rebel.snd");
                sui.msgbox(self, self, allHelpData);//, sui.OK_ONLY, TYRONECOMMANDTITLE);//, "noHandler");
		}
		if (strText.equals("tyronesTestQuests"))
		{
			groundquests.clearQuest(self, "tyrone_clothes");
			groundquests.clearQuest(self, "tyrone_reward");
			groundquests.grantQuest(self, "tyrones_clothes");
			groundquests.grantQuest(self, "tyrone_reward");
		}
		if (strText.equals("chatTest"))
		{
			obj_id objTarget = getLookAtTarget(self);
            chat.chat(objTarget, "Hello!");
            doAnimationAction(objTarget, anims.PLAYER_CURTSEY);
		}
        if (strText.equals("grantAllSkillsCombat"))
        {
            grantAllSkills(self, SPY);
            grantAllSkills(self, OFFICER);
            grantAllSkills(self, COMMANDO);
            grantAllSkills(self, BH);
            grantAllSkills(self, SMUGGLER);
            grantAllSkills(self, JEDI);
            grantAllSkills(self, MEDIC);
        }
        if (strText.equals("grantAllSkillsEntertainer"))
        {
            grantAllSkills(self, ENTERTAINER);
        }
        if (strText.equals("grantAllSkillsCrafting"))
        {
            grantAllSkills(self, DOMESTICS);
            grantAllSkills(self, MUNITIONS);
            grantAllSkills(self, ENGINEER);
            grantAllSkills(self, STRUCTURE);
        }
        if (strText.equals("grantAllSkills"))
        {
            grantAllSkills(self, SPY);
            grantAllSkills(self, OFFICER);
            grantAllSkills(self, COMMANDO);
            grantAllSkills(self, BH);
            grantAllSkills(self, SMUGGLER);
            grantAllSkills(self, JEDI);
            grantAllSkills(self, MEDIC);
            grantAllSkills(self, ENTERTAINER);
            grantAllSkills(self, DOMESTICS);
            grantAllSkills(self, MUNITIONS);
            grantAllSkills(self, ENGINEER);
            grantAllSkills(self, STRUCTURE);
        }
		if (strText.equals("setInteresting"))
		{
            setCondition(getLookAtTarget(self), CONDITION_INTERESTING);
            sendSystemMessageTestingOnly(self, "Set object as interesting.");
        }
		if (strText.equals("persist"))
		{
			obj_id target = getLookAtTarget(self);
            if (target != null && target != obj_id.NULL_ID)
            {
                persistObject(target);
				sendSystemMessageTestingOnly(self, "Persisted: " + target + "!");
            }
		}
		if (strText.equals("clearInteresting"))
		{
            clearCondition(getLookAtTarget(self), CONDITION_INTERESTING);
            sendSystemMessageTestingOnly(self, "Cleared object of interesting tag.");
        }
		if (strText.equals("setSpaceInteresting"))
		{
            setCondition(getLookAtTarget(self), CONDITION_SPACE_INTERESTING);
            sendSystemMessageTestingOnly(self, "Set object as space interesting.");
        }
		if (strText.equals("clearSpaceInteresting"))
		{
            clearCondition(getLookAtTarget(self), CONDITION_SPACE_INTERESTING);
            sendSystemMessageTestingOnly(self, "Cleared object as space interesting.");
        }
        if (strText.equals("makeTyroneADroid"))
        {
            obj_id inventoryContainer = getObjectInSlot(self, "inventory");
            if (!isIdValid(inventoryContainer))
            {
                sendSystemMessageTestingOnly(self, "looks like the objid of the player inventory is invalid");
            }
            obj_id deed = createObject("object/tangible/deed/pet_deed/deed_r2_basic.iff", inventoryContainer, "SLOT_INVENTORY");
            if (!isIdValid(deed))
            {
                sendSystemMessageTestingOnly(self, "failed to create the deed object");
            }
            int powerLevel = 100;
            final String CREATURE_NAME = "r2_crafted";
            dictionary creatureDict = dataTableGetRow(create.CREATURE_TABLE, "r2_crafted");
            setObjVar(deed, "creature_attribs.type", "r2_crafted");
            setObjVar(deed, "creature_attribs.level", 85);
            setObjVar(deed, "creature_attribs.maxHealth", 120000);
            setObjVar(deed, "creature_attribs.maxConstitution", 12200);
            setObjVar(deed, "creature_attribs.general_protection", 20000);
            setObjVar(deed, "creature_attribs.toHitChance", 90);
            setObjVar(deed, "creature_attribs.defenseValue", 90);
            setObjVar(deed, "creature_attribs.minDamage", 1010);
            setObjVar(deed, "creature_attribs.maxDamage", 2284);
            setObjVar(deed, "creature_attribs.aggroBonus", 1.0f);
            setObjVar(deed, "creature_attribs.critChance", 1.0f);
            setObjVar(deed, "creature_attribs.critSave", 1.0f);
            setObjVar(deed, "creature_attribs.scale", 1.1f);
            setObjVar(deed, "creature_attribs.stateResist", 1.0f);
            setObjVar(deed, "crafting_components.cmbt_module", 600.0f);
            setObjVar(deed, "dataModuleRating", 12);
            setObjVar(deed, "storageModuleRating", 12);
            setObjVar(deed, "ai.pet.hasContainer", 12);
            setObjVar(deed, "ai.pet.isRepairDroid", true);
            setObjVar(deed, "craftingStationSpace", true);
            setObjVar(deed, "craftingStationWeapon", true);
            setObjVar(deed, "craftingStationFood", true);
            setObjVar(deed, "module_data.quickset_metal", true);
        }
        return SCRIPT_CONTINUE;
    }
	public void grantAllSkills(obj_id objPlayer, String[] strSkillList) throws InterruptedException
    {
        for (int intI = 0; intI < strSkillList.length; intI++)
        {
            grantSkill(objPlayer, strSkillList[intI]);
        }
    }
}